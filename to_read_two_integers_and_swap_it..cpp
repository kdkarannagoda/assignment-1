#include<stdio.h>
#include<conio.h>

int swap(int *n1,int *n2);//Swap function

int main(){
	int num1,num2;
	printf("Enter 1st Integer : ");
	scanf("%i",&num1);//To read 1st integer value
	
	printf("Enter 2nd Integer : ");
	scanf("%i",&num2);//To read 2nd integer value
	
	printf("Before: %i\t%i\n",num1,num2);//To print 1st & 2nd Integer value before swapping.
	
	swap(&num1,&num2);//To send both integer variable addresses to function
	
	printf("After : %i\t%i",num1,num2);//To print 1st & 2nd Integer value after swapping.
	
	return 0;
}

int swap(int *n1,int *n2){
	int temp; //temp variable for store the value of integer 1
	temp=*n1;	//changing values
	*n1=*n2;	//changing values
	*n2=temp;  //changing values
	return 0;
}
