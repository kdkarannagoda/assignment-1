#include<stdio.h>
#include<math.h>


int main(){
	
	float radius,ans;
	
	printf("Enter the radius of the Disk : ");
	
	scanf("%f",&radius); //To Read radius of the disk
	
	ans = 3.14159*radius*radius; //To calculate Area
	
	printf("Area of the Disk is %.2f",ans); //Print Answer on the Screen
	
	return 0;
	
}
